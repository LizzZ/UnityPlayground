﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering.Universal.PostProcessing; // URP built-in PostProcessing utilities
using UnityEngine.Rendering.PostProcessing; // PostProcessing v2 stack

// Define the Volume Component for the custom post processing effect 
[Serializable, VolumeComponentMenu("CustomPostProcess/Outline")]
public class OutlineEffect : VolumeComponent
{
    [Range(1f, 5f), Tooltip("Outline thickness.")]
    public UnityEngine.Rendering.IntParameter thickness = new UnityEngine.Rendering.IntParameter(2);

    [Range(0f, 5f), Tooltip("Outline edge start.")]
    public UnityEngine.Rendering.FloatParameter edge = new UnityEngine.Rendering.FloatParameter(0.1f);

    [Range(0f, 1f), Tooltip("Outline smoothness transition on close objects.")]
    public UnityEngine.Rendering.FloatParameter transitionSmoothness = new UnityEngine.Rendering.FloatParameter(0.2f);

    [Tooltip("Outline color.")]
    public UnityEngine.Rendering.ColorParameter color = new UnityEngine.Rendering.ColorParameter(Color.black);
}


[CustomPostProcess("Outline",
    CustomPostProcessInjectionPoint.BeforePostProcess | CustomPostProcessInjectionPoint.AfterPostProcess)]
public class OutlineEffectRenderer : CustomPostProcessRenderer
{
   
    // A variable to hold a reference to the corresponding volume component
    private OutlineEffect m_VolumeComponent;

    // The postprocessing material
    private Material m_Material;

    // The ids of the shader variables
    static class ShaderIDs
    {
        internal readonly static int Input = Shader.PropertyToID("_MainTex");
        internal readonly static int CameraDepthTexture = Shader.PropertyToID("_CameraDepthTexture");
        internal readonly static int Thickness = Shader.PropertyToID("_Thickness");
        internal readonly static int TransitionSmoothness = Shader.PropertyToID("_TransitionSmoothness");
        internal readonly static int Edge = Shader.PropertyToID("_Edge");
        internal readonly static int Color = Shader.PropertyToID("_Color");
    }

    // By default, the effect is visible in the scene view, but we can change that here.
    public override bool visibleInSceneView => true;

    /// Specifies the input needed by this custom post process. Default is Color only.
    public override ScriptableRenderPassInput input => ScriptableRenderPassInput.Color;

    // Initialized is called only once before the first render call
    // so we use it to create our material
    public override void Initialize()
    {
        m_Material = CoreUtils.CreateEngineMaterial("Hidden/PostProcess/Outline");
    }

    // Called for each camera/injection point pair on each frame. Return true if the effect should be rendered for this camera.
    public override bool Setup(ref RenderingData renderingData, CustomPostProcessInjectionPoint injectionPoint)
    {
        // Get the current volume stack
        var stack = VolumeManager.instance.stack;
        // Get the corresponding volume component
        m_VolumeComponent = stack.GetComponent<OutlineEffect>();
        // if blend value > 0, then we need to render this effect. 
        return m_VolumeComponent.thickness.value > 0;
    }

 
    // The actual rendering execution is done here
    public override void Render(CommandBuffer cmd, RenderTargetIdentifier source, RenderTargetIdentifier destination,
        ref RenderingData renderingData, CustomPostProcessInjectionPoint injectionPoint)
    {
        // set material properties
        if (m_Material != null)
        {
            m_Material.SetInt(ShaderIDs.Thickness, m_VolumeComponent.thickness.value);
            m_Material.SetFloat(ShaderIDs.TransitionSmoothness, m_VolumeComponent.transitionSmoothness.value);
            m_Material.SetFloat(ShaderIDs.Edge, m_VolumeComponent.edge.value);
            m_Material.SetColor(ShaderIDs.Color, m_VolumeComponent.color.value);
        }
        // Get PropertySheet
        var sheetFactory = new PropertySheetFactory();
        var sheet = sheetFactory.Get(m_Material.shader);
        sheet.properties.SetInt(ShaderIDs.Thickness, m_VolumeComponent.thickness.value);
        sheet.properties.SetFloat(ShaderIDs.TransitionSmoothness, m_VolumeComponent.transitionSmoothness.value);
        sheet.properties.SetFloat(ShaderIDs.Edge, m_VolumeComponent.edge.value);
        sheet.properties.SetColor(ShaderIDs.Color, m_VolumeComponent.color.value);
        
        // draw a fullscreen triangle to the destination
        cmd.BlitFullscreenTriangle(source, destination, sheet, 0);
    }
}